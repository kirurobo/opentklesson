﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// OpenTKの使用を宣言
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;


namespace OpenTKsample01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// OpenGLの座標系を1px=1.0単位に合わせる
        /// </summary>
        private void SetViewport()
        {
            // GLコントロールのどの領域を使うか指定（GLコントロール全体を指定している）
            GL.Viewport(0, 0, glControl1.Width, glControl1.Height);

            // 座標系を決める際の最初に呼ぶ（今のところおまじないと思っておいても結構です）
            GL.LoadIdentity();

            // OpenGL座標系での 1.0単位 を画面の 1px に対応させる（厳密には違う可能性があるけど）
            double halfW = glControl1.Width / 2.0;
            double halfH = glControl1.Height / 2.0;
            GL.Ortho(-halfW, halfW, -halfH, halfH, -1.0, 1.0);
           
        }

        /// <summary>
        /// GLコントロールの描画要求時に呼ばれる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);   // 背景色で描画領域を初期化

            double val;                     // 輝度

            GL.Begin(BeginMode.Points);     // 描画開始
            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    val = i / 100.0;            // X座標によって輝度を決める
                    GL.Color3(0, 0, val);   // 色を設定（明るさ val の灰色）
                    GL.Vertex2(i, j);           // 点を打つ
                }
            }
            GL.End();                       // 描画終了

            glControl1.SwapBuffers();       // 表画面と裏画面を入れ替え。この時点で表示される。
        }

        /// <summary>
        /// フォームが読み込まれた際に呼ばれる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            SetViewport();                  // 画面の領域や座標系の初期化

            GL.ClearColor(Color.DarkGray);  // 背景色を設定。講義では未登場ですけどね(^_^)
        }

        /// <summary>
        /// GLコントロールのサイズ変更時に呼ばれる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_Resize(object sender, EventArgs e)
        {
            SetViewport();              // OpenGL座標系を再定義

            glControl1.Invalidate();    // GLコントロールの再描画を求める
        }
    }
}
